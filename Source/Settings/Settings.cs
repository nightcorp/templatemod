﻿using HarmonyLib;
using NightmareCore;
using NightmareCore.Settings;
using NightmareCore.Utilities;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Networking;
using Verse;

namespace TEMPLATE
{
    public class TemplateMod : SmartMod
    {
        public static TemplateMod Mod { get; private set; }
        public static TemplateModSettings Settings { get; private set; }
        public override string SettingsCategory() => "Aqua_Settings_Title".Translate();
        public TemplateMod(ModContentPack content) : base(content)
        {
            Settings = GetSettings<TemplateModSettings>();
            Mod = this;
            InternalSettings = Settings;
        }
    }

    public class TemplateModSettings : SmartModSettings
    {
        // SmartModSettingsTab subclasses will be retrieved and instantiated via reflection
        TemplateTab presetTab;

        public override void DefsLoaded()
        {
            base.DefsLoaded();
        }
    }

    public class TemplateTab : SmartModSettingsTab
    {
        protected override string TabTranslationKey => "Title";

        public override void DrawContent(Rect inRect)
        {
            UIUtility.Label(inRect, "test", GameFont.Medium, TextAnchor.MiddleCenter);
        }
    }
}
